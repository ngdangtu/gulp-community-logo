# Gulp Community Logo
![Gulp Community Logo](./source.inkscape.svg)

## Meaning
The community logo is a beer mug. This image was chosen because beer thingy usually drunk when people get together in a pub. Which is a community in somewhat meaning it could offer, getting together. Another reason for beer glasses is to inherit the original Gulp logo, a drinking cup. And finally, coding can be addictive as drinking alcohol.


## Reference
These below are material used for idea. The logo is completely drawed from scratch.

- Refer directory `/references`.
- [10 Types of Beer Glasses to Complement Your Beer](https://learn.kegerator.com/beer-glasses)